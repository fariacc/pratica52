import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade TecnolÃ³gica Federal do ParanÃ¡
 * DAINF - Departamento AcadÃªmico de InformÃ¡tica
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */

public class Pratica52 {
public static void main(String[] args) {
        Equacao2Grau<Integer> equacao1 = new Equacao2Grau<>(4, 6, 3);
        Equacao2Grau<Integer> equacao2 = new Equacao2Grau<>(3, 4, 5);
        Equacao2Grau<Integer> equacao3 = new Equacao2Grau<>(2, 7, 1);

        try {
            System.out.println("Equacao 1 raiz 1: " + equacao1.getRaiz1());
            System.out.println("Equacao 1 raiz 2: " + equacao1.getRaiz2());

        } catch (Exception e) {
            System.out.println("Erro: " + e);
        } finally {
            try {
                System.out.println("Equacao 2 raiz 1: " + equacao2.getRaiz1());
                System.out.println("Equacao 2 raiz 2: " + equacao2.getRaiz2());

            } catch (Exception e) {
                System.out.println("Erro: " + e);
            } finally {
                try {
                    System.out.println("Equacao 3 raiz 1: " + equacao3.getRaiz1());
                    System.out.println("Equacao 3 raiz 2: " + equacao3.getRaiz2());
                } catch (Exception e) {
                    System.out.println("Erro: " + e);
                }
            }
        }

    }
}
