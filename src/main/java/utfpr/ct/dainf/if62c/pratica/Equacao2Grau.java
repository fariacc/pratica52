/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author MHFM
 * @param <T>
 */
public class Equacao2Grau<T extends Number> {

    T a;
    T b;
    T c;

    public Equacao2Grau(T a, T b, T c) {
        if (a.equals(0)) {
            throw new RuntimeException("Coeficiente a não pode ser zero");
        }

        this.a = a;
        this.b = b;
        this.c = c;

    }

    public void setA(T a) {
        if (a.equals(0) || a.equals(0.0)) {
            throw new RuntimeException("Coeficiente a não pode ser zero");
        }

        this.a = a;
    }

    public void setB(T b) {
        this.b = b;
    }

    public void setC(T c) {
        this.c = c;
    }

    public T getA() {
        return a;
    }

    public T getB() {
        return b;
    }

    public T getC() {
        return c;
    }

    public double getRaiz1() {
        double cA = this.a.doubleValue();
        double cB = this.b.doubleValue();
        double cC = this.c.doubleValue();
        double delta = Math.pow(cB, 2) - 4 * cA * cC;

        if (delta < 0) {
            throw new RuntimeException("Equação não tem solução real");
        } else {
            return (-cB + Math.sqrt(delta)) / (2 * cA);
        }
    }

    public double getRaiz2() {
        double cA = this.a.doubleValue();
        double cB = this.b.doubleValue();
        double cC = this.c.doubleValue();
        double delta = Math.pow(cB, 2) - 4 * cA * cC;

        if (delta < 0) {
            throw new RuntimeException("Equação não tem solução real");
        } else {
            return (-cB - Math.sqrt(delta)) / (2 * cA);
        }
    }
}